import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from '@core/models/user';
import { Observable, map, switchMap, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private afs: AngularFirestore) { }

  addUser(user: User) {
    const id = this.afs.createId();
    return this.afs.collection('users').add(user);
  }

  findUser(user: User): Observable<any[]> {
    return this.afs.collection('users', ref => ref.where('email', '==', user.email)).valueChanges();
  }

  getLocations(): Observable<any[]> {
    return this.afs.collection('locations').valueChanges().pipe(
      switchMap((actions: any[]) => {
        const observables = actions.map(action => {
          const data = action.proposition.map((ref: any) => {
            return ref.get().then((doc: any) => doc.data());
          });

          return Promise.all(data).then((propositionsData: any[]) => {
            return { ...action, proposition: propositionsData };
          });
        });

        return forkJoin(observables);
      })
    );
  }
}
