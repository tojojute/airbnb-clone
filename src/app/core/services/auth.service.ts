import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { User } from '@core/models/user';
import { DataService } from './data.service';
import { map, take } from 'rxjs';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fireauth: AngularFireAuth, private dataService: DataService, private toastService: ToastService) { }
  stopSign: boolean = false;
  login(user: User): Promise<void> {
    if (!this.stopSign) {
      return new Promise<void>((resolve, reject) => {
        this.fireauth.signInWithEmailAndPassword(user.email, user.password).then(() => {
          this.status();
          resolve();
          console.log('login user success');
          this.stopSign = true;
        }).catch((err) => {
          console.log(err);
          this.stopSign = true;
          reject();
        }).finally(() => {
          setTimeout(() => {
            this.stopSign = true;
          }, 1000);
        });
      });
    } else {
      return new Promise<void>(() => null)
    }
  }

  register(user: User): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.fireauth.createUserWithEmailAndPassword(user.email, user.password).then(() => {
        this.dataService.addUser(user).then(() => {
          alert('Inscription effectuée');
          resolve(); // Résoudre la promesse en cas de succès
        }).catch((e: any) => {
          this.toastService.show({
            message: "Une erreur s'est produit lors de l'inscription",
            type: 'ERROR'
          });
          reject('Une erreur s\'est produite lors de l\'ajout de l\'utilisateur'); // Rejeter la promesse en cas d'échec
        });
      }).catch((err) => {
        this.toastService.show({
          message: "Une erreur s'est produit lors de l'inscription.",
          type: 'ERROR'
        });
        console.log('register auth user failed');
        reject('Une erreur s\'est produite lors de l\'authentification de l\'utilisateur'); // Rejeter la promesse en cas d'échec
      });
    });
  }


  verifyEmailIfExist(user: User): Promise<string[]> {
    return this.fireauth.fetchSignInMethodsForEmail(user.email);
  }

  logout() {
    this.fireauth.signOut().then(() => {
      localStorage.removeItem('token');
      this.toastService.show({
        message: "Vous êtes déconnecté(e)",
        type: 'SUCCESS'
      });
    }, err => {
      this.toastService.show({
        message: "Erreur de déconnexion",
        type: 'ERROR'
      });
    });
  }

  status() {
    this.fireauth.authState.pipe(
      take(1),
      map((user: any) => {
        if (!user) {
          localStorage.removeItem('token');
        } else {
          localStorage.setItem('token', user._delegate.accessToken)
        }
      })
    ).subscribe();
  }
}
