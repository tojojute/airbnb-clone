import { Injectable } from '@angular/core';
import { Toast } from '@core/models/toast';
import { ToastComponent } from '@shared/reusableComponents/toast/toast.component';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private monToast: ToastComponent | null = null;

  constructor() { }

  add(toast: ToastComponent) {
    this.monToast = toast;
  }

  show(data: Toast) {
    if (this.monToast) {
      this.monToast.show(data);
    }
  }
}
