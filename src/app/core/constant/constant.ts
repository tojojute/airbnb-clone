import { Menu } from "../models/menu";

const url1 = 'https://airbnb13.p.rapidapi.com';
export const API = {
  'search_location': `${url1}/search-location`
};

export const firebaseConfig = {
  apiKey: "AIzaSyAE-5tzddT54YK2XbSpVhmAn3KocbuMhKA",
  authDomain: "airbnb-clone-7f36c.firebaseapp.com",
  projectId: "airbnb-clone-7f36c",
  storageBucket: "airbnb-clone-7f36c.appspot.com",
  messagingSenderId: "223826215037",
  appId: "1:223826215037:web:373bd47abbaf9fe317f114"
};

export const MENU: Menu[] = [
  {
    icon: 'assets/icons/coffee.jpg',
    title: "Chambres d\'hôtes",
    isActive: true
  },
  {
    icon: 'assets/icons/tropic.jpg',
    title: "Sous les tropiques",
    isActive: false
  },
  {
    icon: 'assets/icons/ile.jpg',
    title: "Iles",
    isActive: false
  },
  {
    icon: 'assets/icons/ville.jpg',
    title: "Villes emblématiques",
    isActive: false
  },
  {
    icon: 'assets/icons/piscine.jpg',
    title: "Piscines",
    isActive: false
  },
  {
    icon: 'assets/icons/ferme.jpg',
    title: "Fermes",
    isActive: false
  },
  {
    icon: 'assets/icons/piste.jpg',
    title: "Au pied des pistes",
    isActive: false
  },
  {
    icon: 'assets/icons/arctique.jpg',
    title: "Arctique",
    isActive: false
  },
  {
    icon: 'assets/icons/key.jpg',
    title: "Nouveautés",
    isActive: false
  },
  {
    icon: 'assets/icons/caravane.jpg',
    title: "Caravanes",
    isActive: false
  },
  {
    icon: 'assets/icons/wow.jpg',
    title: "Wow",
    isActive: false
  },
  {
    icon: 'assets/icons/surf.jpg',
    title: "Surf",
    isActive: false
  },
  {
    icon: 'assets/icons/vue.jpg',
    title: "Avec vue",
    isActive: false
  },
  {
    icon: 'assets/icons/design.jpg',
    title: "Design",
    isActive: false
  },
  {
    icon: 'assets/icons/parc.jpg',
    title: "Parc nationaux",
    isActive: false
  },
  {
    icon: 'assets/icons/cabane.jpg',
    title: "Cabanes",
    isActive: false
  },
  {
    icon: 'assets/icons/troglodyte.jpg',
    title: "Maison troglodytes",
    isActive: false
  },
  {
    icon: 'assets/icons/plage.jpg',
    title: "Plages",
    isActive: false
  },
  {
    icon: 'assets/icons/chambre.jpg',
    title: "Chambres",
    isActive: false
  },
  {
    icon: 'assets/icons/tendance.jpg',
    title: "Tendance",
    isActive: false
  },
  {
    icon: 'assets/icons/chateau.jpg',
    title: "Châteaux",
    isActive: false
  },
  {
    icon: 'assets/icons/flour.jpg',
    title: "Maisons organiques",
    isActive: false
  }
]

export const CARTE: Menu[] = [
  {
    icon: 'https://a0.muscache.com/pictures/f9ec8a23-ed44-420b-83e5-10ff1f071a13.jpg',
    title: 'Je suis flexible'
  },
  {
    icon: 'https://a0.muscache.com/im/pictures/7e9673a5-4164-4708-a047-8d281b5980e7.jpg?im_w=320',
    title: 'Afrique'
  },
  {
    icon: 'https://a0.muscache.com/im/pictures/ea5598d7-2b07-4ed7-84da-d1eabd9f2714.jpg?im_w=320',
    title: 'Italie'
  },
  {
    icon: 'https://a0.muscache.com/im/pictures/d77de9f5-5318-4571-88c7-e97d2355d20a.jpg?im_w=320',
    title: 'Asie du Sud-Est'
  },
  {
    icon: 'https://a0.muscache.com/im/pictures/d2309871-490d-452f-a61c-35b19ad3d75e.jpg?im_w=320',
    title: 'Afrique du sud'
  },
  {
    icon: 'https://a0.muscache.com/im/pictures/66355b01-4695-4db9-b292-c149c46fb1ca.jpg?im_w=320',
    title: 'Moyen-Orient'
  },
]
