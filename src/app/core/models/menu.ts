export interface Menu {
  icon: string,
  title: string,
  isActive?: boolean
}
export interface FiltreMenu {
  destination: {
    isActive: boolean,
    value?: string
  },
  arrive: {
    isActive: boolean,
    value?: string
  },
  depart: {
    isActive: boolean,
    value?: string
  },
  age: {
    isActive: boolean,
    value?: string
  },
  profil: {
    isActive: boolean,
    value: string
  }
}
export interface FiltreAge {
  title: string,
  description: string,
  value: number
}