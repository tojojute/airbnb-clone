export interface Location {
  title: string,
  subtitle: string,
  description: string,
  images: Array<string>,
  room: number,
  toilet: number,
  voyageur: number,
  bed: number,
  bathroom: number,
  proposition: Proposition[],
  price: number
}

export interface Proposition {
  description: string,
  icon: string
}