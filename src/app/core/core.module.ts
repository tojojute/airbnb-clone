import { NgModule } from "@angular/core";
import { CheckUserGuard } from "./guard/check-user.guard";

@NgModule({
  declarations: [],
  providers: [CheckUserGuard]
})
export class CoreModule { }
