import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'randomList'
})
export class RandomListPipe implements PipeTransform {
  transform<T>(array: T[]): T[] {
    let currentIndex = array.length, randomIndex;

    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }

    return array;
  }
}
