import { Component, Input, OnInit } from '@angular/core';
import { Toast } from '@core/models/toast';
import { ToastService } from '@core/services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})


export class ToastComponent implements OnInit {
  // @Input() toast!: Toast;
  open: boolean = false;
  toast: Toast = {
    message: '',
    type: 'DANGER'
  }

  constructor(private toastService: ToastService) { }

  ngOnInit(): void {
    this.toastService.add(this);
  }
  show(data: Toast) {
    this.open = true;
    this.toast = data;
  }

  close() {
    this.open = false;
    this.toast = {
      message: '',
      type: 'DANGER'
    };
  }
}
