import { Component, Input } from '@angular/core';
import { FiltreAge } from '@core/models/menu';

@Component({
  selector: 'modal-age',
  templateUrl: './modal-age.component.html',
  styleUrls: ['./modal-age.component.scss']
})
export class ModalAgeComponent {
  @Input() showModal: boolean = false;

  age: FiltreAge[] = [
    {
      title: 'Adultes',
      description: '13 ans et plus',
      value: 0
    },
    {
      title: 'Enfants',
      description: 'De 2 à 12 ans',
      value: 0
    },
    {
      title: 'Bébés',
      description: '- de 2 ans',
      value: 0
    },
    {
      title: 'Animaux domestiques',
      description: "Vous voyagez avec un animal d'assistance?",
      value: 0
    }
  ]

  increment(item: FiltreAge) {
    if (item.value < 16) {
      item.value++;
    }
  }

  decrement(item: FiltreAge) {
    if (item.value > 0) {
      item.value--;
    }
  }
}
