import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAgeComponent } from './modal-age.component';

describe('ModalAgeComponent', () => {
  let component: ModalAgeComponent;
  let fixture: ComponentFixture<ModalAgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAgeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalAgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
