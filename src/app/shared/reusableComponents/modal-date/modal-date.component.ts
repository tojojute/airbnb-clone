import { Component, Input } from '@angular/core';

@Component({
  selector: 'modal-date',
  templateUrl: './modal-date.component.html',
  styleUrls: ['./modal-date.component.scss']
})
export class ModalDateComponent {
  @Input() showModal: boolean = false;

  constructor() { }

  close() {
    this.showModal = false;
  }
}
