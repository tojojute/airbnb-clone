import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { CARTE } from '@core/constant/constant';
import { Menu } from '@core/models/menu';
import '@doubletrade/lit-datepicker';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @Input() showModal: boolean = false;
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>;
  carte: Menu[] = CARTE;

  constructor(private el: ElementRef) { }

  close() {
    this.showModal = false;
    this.closeModal.emit();
  }

}
