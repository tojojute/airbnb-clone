import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() data: string[] = [];
  currentSlideIndex: number = 0;
  btnLeft: boolean = false;
  btnRight: boolean = true;

  ngOnInit() {
    this.check();
    console.log(this.data);

  }

  changeSlide(index: number) {
    this.currentSlideIndex = index;
  }

  nextSlide() {
    this.currentSlideIndex = (this.currentSlideIndex + 1) % this.data.length;
    this.check();
  }

  prevSlide() {
    this.currentSlideIndex = (this.currentSlideIndex - 1 + this.data.length) % this.data.length;
    this.check();
  }
  check() {
    this.btnLeft = this.currentSlideIndex != 0;
    this.btnRight = this.currentSlideIndex != (this.data.length - 1);
  }

}
