import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Modal } from 'flowbite';
import { AuthComponent } from '../auth/auth.component';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'menu-profil',
  templateUrl: './menu-profil.component.html',
  styleUrls: ['./menu-profil.component.scss']
})
export class MenuProfilComponent implements OnInit {
  @Input() showModal: boolean = false;
  @Output() active = new EventEmitter<boolean>();

  test: boolean = true;
  @ViewChild(AuthComponent, { static: false }) modalAuth!: AuthComponent;
  isLogin: boolean = false;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.isLogin = localStorage.getItem('token') ? true : false;
  }

  openModal() {
    const $targetEl = document.getElementById('authentication-modal');
    const options = {
      backdropClasses:
        'bg-gray-900/50 dark:bg-gray-900/80 fixed inset-0 z-40',
      closable: true,
      onHide: () => {
        this.test = true;
        this.active.emit(false);
      },
      onShow: () => {
        this.test = false;
      }
    };

    // instance options object
    const instanceOptions = {
      id: 'authentication-modal',
      override: true
    };
    const modal = new Modal($targetEl, options, instanceOptions);
    modal.show();
  }

  logout() {
    this.auth.logout();
    this.test = false;
    this.active.emit(false);
  }

  actives($event: boolean) {
    console.log('aaa' + $event);

    this.active.emit(false);
  }
}
