import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Menu } from '@core/models/menu';
import { User } from '@core/models/user';
import { AuthService } from '@core/services/auth.service';
import { DataService } from '@core/services/data.service';
import { ToastService } from '@core/services/toast.service';
import { Modal } from 'flowbite';
import { of, switchMap, take, tap } from 'rxjs';

@Component({
  selector: 'modal-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {
  socialMedia: Menu[] = [
    {
      title: 'Continuer avec Facebook',
      icon: 'assets/icons/fb.svg'
    },
    {
      title: 'Continuer avec Google',
      icon: 'assets/icons/google.svg'
    },
    {
      title: 'Continuer avec Apple',
      icon: 'assets/icons/apple.svg'
    }
    ,
    {
      title: 'Continuer avec le numéro de téléphone',
      icon: 'assets/icons/phone.svg'
    }
  ];
  isLogin: boolean = false;
  @Output() active = new EventEmitter<boolean>();

  constructor(private auth: AuthService, private data: DataService, private toastService: ToastService) { }
  title: string = 'Connexion ou inscription';
  isRegister: boolean = false;
  loading: boolean = false;

  user: User = {
    email: '',
    password: '',
    birthday: '',
    firstname: '',
    lastname: ''
  };

  loginView() {
    this.isRegister = false;
    this.title = 'Connexion ou inscription';
  }
  register(type: string) {
    console.log(this.user);
    this.loading = true;
    if (this.user.password.length >= 6) {
      switch (type) {
        case 'email':
          this.auth.register(this.user).then(() => {
            this.loading = false;
            this.toastService.show({
              message: 'Inscription effectuée',
              type: 'SUCCESS'
            });
            let $targetEl = document.getElementById('authentication-modal');
            if ($targetEl) {
              const modal = new Modal($targetEl);
              this.active.emit(false);
              modal.hide();
            }
          });
          break;
        default:
          console.log('default');
      }
    }
  }

  verifyEmailLogin() {
    this.data.findUser(this.user).pipe(
      take(1)
    ).subscribe((users => {
      const emailExists = users.some(user => user.email === this.user.email);
      if (this.isLogin) {
        this.loading = true;
        this.auth.login(this.user).then(() => {
          this.loading = false;
          this.toastService.show({
            message: 'Vous êtes connecté(e) avec succès',
            type: 'SUCCESS'
          });
          let $targetEl = document.getElementById('authentication-modal');
          if ($targetEl) {
            const modal = new Modal($targetEl);
            this.active.emit(false);
            modal.hide();
          }
        })
      } else {
        if (!emailExists) {
          this.isRegister = true;
          this.title = 'Terminer mon inscription';
          this.isLogin = false;
        } else {
          this.isLogin = true;
        }
      }
    }));
  }

}
