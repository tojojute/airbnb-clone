import { Component, Input } from '@angular/core';
import { Location } from '@core/models/location';

@Component({
  selector: 'card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent {
  @Input() data!: Location[];
  imgs: string[] = [
    'https://a0.muscache.com/im/pictures/miso/Hosting-859709719997723808/original/ca07821e-c540-41a5-8e89-1daf0ea9e9a6.jpeg?im_w=720',
    'https://a0.muscache.com/im/pictures/miso/Hosting-52960006/original/6e21b2e3-4a50-44f7-9641-dc9ae2e2ef4e.jpeg?im_w=720',
    'https://a0.muscache.com/im/pictures/miso/Hosting-19712496/original/6e648ff8-9fbc-4a15-bb25-df2b525af4df.jpeg?im_w=720'
  ];
}
