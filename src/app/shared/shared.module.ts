import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { CardListComponent } from './reusableComponents/card-list/card-list.component';
import { RandomListPipe } from './pipes/random-list.pipe';
import { CarouselComponent } from './reusableComponents/carousel/carousel.component';
import { ModalComponent } from './reusableComponents/modal/modal.component';
import { ModalDateComponent } from './reusableComponents/modal-date/modal-date.component';
import { ModalAgeComponent } from './reusableComponents/modal-age/modal-age.component';
import { MenuProfilComponent } from './reusableComponents/menu-profil/menu-profil.component';
import { AuthComponent } from './reusableComponents/auth/auth.component';
import { FormsModule } from '@angular/forms';
import { ToastComponent } from './reusableComponents/toast/toast.component';

@NgModule({
  declarations: [
    CardListComponent,
    RandomListPipe,
    CarouselComponent,
    ModalComponent,
    ModalDateComponent,
    ModalAgeComponent,
    MenuProfilComponent,
    AuthComponent,
    ToastComponent
  ],
  exports: [RandomListPipe, CardListComponent, ModalComponent, ModalDateComponent, ModalAgeComponent, MenuProfilComponent, AuthComponent, CarouselComponent, ToastComponent],
  imports: [BrowserModule, FormsModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class SharedModule { }
