import { Component, HostListener, Inject, Injector, OnInit } from '@angular/core';
import { ToastService } from '@core/services/toast.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'airbnb-clone';

  constructor(private toastService: ToastService) { }

  ngOnInit(): void {
    // this.auth.status();
    // setTimeout(() => {
    //   this.toastService.show({
    //     message: 'fsdqfds',
    //     type: 'SUCCESS'
    //   });
    // }, 1000);
  }

  isScrolled: boolean = false;
  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event): void {
    this.isScrolled = window.scrollY > 6;
  }
}
