import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckUserGuard } from '@core/guard/check-user.guard';
import { HomeComponent } from '@views/home/home.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    canActivate: [CheckUserGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
