import { Component, HostListener, OnInit } from '@angular/core';
import { DataService } from '@core/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  locations$: Observable<any[]>;

  constructor(private dataService: DataService) {
    this.locations$ = this.dataService.getLocations();
  }

  isScrolled: boolean = false;
  @HostListener('window:scroll', ['$event'])
  onScroll(event: Event): void {
    this.isScrolled = window.scrollY > 6;
  }

}
