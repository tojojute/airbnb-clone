import { Component, ElementRef, Input, ViewChild, HostListener } from '@angular/core';
import { MENU } from '@core/constant/constant';
import { FiltreMenu, Menu } from '@core/models/menu';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  menuView: Menu[] = MENU;
  orientation: number = -1;
  btnRight: boolean = false;
  btnLeft: boolean = false;
  showMenu: FiltreMenu = {
    destination: {
      isActive: false,
      value: ''
    },
    arrive: {
      isActive: false,
      value: ''
    },
    depart: {
      isActive: false,
      value: ''
    },
    age: {
      isActive: false,
      value: ''
    },
    profil: {
      isActive: false,
      value: ''
    }
  };
  activeSousMenu: string = '';

  @Input() isScrolled: boolean = false;

  constructor(private el: ElementRef) { }

  @ViewChild('scrollContainer', { static: false }) scrollContainer: ElementRef | undefined;
  scrollHorizontal(percentage: number) {
    const container = this.scrollContainer?.nativeElement;
    const totalWidth = container.scrollWidth - container.clientWidth;
    const distance = (totalWidth * percentage) / 100;
    container.scrollLeft += distance;
    this.orientation = distance;
    this.btnRight = container.scrollLeft >= totalWidth;
    this.btnLeft = container.scrollLeft <= 0;
  }

  choiceCategory(menu: Menu) {
    this.menuView.forEach(item => item.isActive = (item.icon === menu.icon));
  }

  openMenu(type: string) {
    this.clearMenu();
    this.showMenu[type as keyof FiltreMenu].isActive = true;
    if (type != 'profil')
      this.activeSousMenu = type;
  }

  closeModal() {
    this.clearMenu();
    this.activeSousMenu = '';
  }

  clearMenu() {
    this.showMenu = {
      destination: {
        isActive: false,
        value: ''
      },
      arrive: {
        isActive: false,
        value: ''
      },
      depart: {
        isActive: false,
        value: ''
      },
      age: {
        isActive: false,
        value: ''
      },
      profil: {
        isActive: false,
        value: ''
      }
    };
  }
  @HostListener('document:click', ['$event'])
  onDocumentClick(event: Event) {
    if (!this.el.nativeElement.contains(event.target) && !this.modalContainsTarget(event.target)) {
      this.clearMenu();
      this.activeSousMenu = '';
    }
  }
  active($event: boolean) {
    this.showMenu.profil.isActive = $event;
  }

  private modalContainsTarget(target: any): boolean {
    return this.el.nativeElement.contains(target);
  }

  stopPropagation(event: Event): void {
    event.stopPropagation();
  }

  showFiltre() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }
}
