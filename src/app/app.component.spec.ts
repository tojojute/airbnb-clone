import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NavComponent } from '@views/layouts/nav/nav.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RandomListPipe } from '@shared/pipes/random-list.pipe';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent, NavComponent, RandomListPipe
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  describe('Detect scroll', () => {
    const simulateScroll = (scrollY: number) => {
      window.scrollY = scrollY;
      window.dispatchEvent(new Event('scroll'));
      fixture.detectChanges();
    };

    it('should be in position fixed if scroll greater than 6px, add "fixed" class if isScrolled is true', () => {
      simulateScroll(10);
      expect(component.isScrolled).toBe(true);
      const element = fixture.nativeElement.querySelector('#top-nav');
      expect(element.classList.contains('fix')).toBe(true);
    });

    it('should not be in position fixed if scroll less than or equal to 6px', () => {
      simulateScroll(5);
      expect(component.isScrolled).toBe(false);
      simulateScroll(6);
      expect(component.isScrolled).toBe(false);
      const element = fixture.nativeElement.querySelector('#top-nav');
      expect(element.classList.contains('fix')).toBe(false);
    });
  });
});
